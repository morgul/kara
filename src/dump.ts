// ---------------------------------------------------------------------------------------------------------------------
// Dump Timestamps
// ---------------------------------------------------------------------------------------------------------------------

import { program } from 'commander';

// Resource Access
import setlistRA from './resource-access/setlist';

// Utils
import { getDescription, getName, getVersion } from './utils/version.js';

// ---------------------------------------------------------------------------------------------------------------------

program
    .name(getName('multiplex'))
    .description(getDescription('Some script.'))
    .version(getVersion('0.0.0'))
    .requiredOption('-f --file <setlist_file>', 'Setlist yaml file');

program.parse();

const { file } = program.opts();

// ---------------------------------------------------------------------------------------------------------------------

async function main() : Promise<void>
{
    // Read in set list file
    const setList = await setlistRA.load(file);

    console.log(`Dumping set list '${ file }':\n`);

    setList.songs.forEach((song) =>
    {
        console.log(`${ song.originalArtist } - ${ song.title }: ${ song.start } - ${ song.end }`);
    });
}

// ---------------------------------------------------------------------------------------------------------------------

main()
    .catch((error) =>
    {
        console.error('Unexpected error, exiting. Error was:', error.stack);
        process.exit(1);
    });

// ---------------------------------------------------------------------------------------------------------------------
