// ---------------------------------------------------------------------------------------------------------------------
// Kara-oke YouTube Downloader
// ---------------------------------------------------------------------------------------------------------------------

import { program } from 'commander';
import logging from '@strata-js/util-logging';

// Engines
import downloadEng from './engines/download';

// Resource Access
import filesRA from './resource-access/files';
import setlistRA from './resource-access/setlist';

// Utils
import { getDescription, getName, getVersion } from './utils/version.js';

// ---------------------------------------------------------------------------------------------------------------------

program
    .name(getName('kara'))
    .description(getDescription('Takes a YouTube video and chop it into multiple mp3s based on a timestamp file.'))
    .version(getVersion('0.0.0'))
    .option('-c --cache', 'Enables a cache for downloaded videos', false)
    .option('-v --video <video_file>', 'Path to pre-downloaded video file')
    .requiredOption('-f --file <setlist_file>', 'Setlist yaml file')
    .requiredOption('-o --output <output_dir>', 'Output directory');

program.parse();

const { file, output, video, cache } = program.opts();

// ---------------------------------------------------------------------------------------------------------------------

const logger = logging.getLogger('kara');

// ---------------------------------------------------------------------------------------------------------------------

async function main() : Promise<void>
{
    logger.info(`Kara v${ getVersion('0.0.0') } started.`);

    // TODO, we can probably just mkdir this...
    if(!await filesRA.exists(output))
    {
        throw new Error(`The path '${ output }' does not exist!`);
    }

    logger.info(`Loading set list from '${ file }'...`);

    // Read in set list file
    const setList = await setlistRA.load(file);

    logger.debug(`Found set list definition with ${ setList.songs.length } songs defined.`);

    // Download the set list
    await downloadEng.downloadSetList(setList, output, video, cache);

    logger.info('Set list downloaded successfully!');
}

// ---------------------------------------------------------------------------------------------------------------------

main()
    .catch((error) =>
    {
        logger.error('Unexpected error, exiting. Error was:', error.stack);
        process.exit(1);
    });

// ---------------------------------------------------------------------------------------------------------------------
