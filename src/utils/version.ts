// ---------------------------------------------------------------------------------------------------------------------
// Version Util
// ---------------------------------------------------------------------------------------------------------------------

import { resolve } from 'node:path';
import { readFileSync } from 'node:fs';

import logging from '@strata-js/util-logging';

// ---------------------------------------------------------------------------------------------------------------------

const logger = logging.getLogger('multiplex');

// ---------------------------------------------------------------------------------------------------------------------

export function getDescription(fallbackDesc = 'Unknown script') : string
{
    let desc : string = process.env.npm_package_description ?? '';
    if(!desc)
    {
        const pgkText = readFileSync(resolve(__dirname, '..', '..', 'package.json'), { encoding: 'utf8' });

        try
        {
            desc = (JSON.parse(pgkText)).description;
        }
        catch (err : any)
        {
            logger.warn(`Failed to parse description from 'package.json':`, err.stack);
            desc = fallbackDesc;
        }
    }

    return desc;
}

export function getName(fallbackName = 'unknown-pkg') : string
{
    let name : string = process.env.npm_package_name ?? '';
    if(!name)
    {
        const pgkText = readFileSync(resolve(__dirname, '..', '..', 'package.json'), { encoding: 'utf8' });

        try
        {
            name = (JSON.parse(pgkText)).name;
        }
        catch (err : any)
        {
            logger.warn(`Failed to parse name from 'package.json':`, err.stack);
            name = fallbackName;
        }
    }

    return name;
}

export function getVersion(fallbackVersion = '0.0.0') : string
{
    let version : string = process.env.npm_package_version ?? '';
    if(!version)
    {
        const pgkText = readFileSync(resolve(__dirname, '..', '..', 'package.json'), { encoding: 'utf8' });

        try
        {
            version = (JSON.parse(pgkText)).version;
        }
        catch (err : any)
        {
            logger.warn(`Failed to parse version from 'package.json':`, err.stack);
            version = fallbackVersion;
        }
    }

    return version;
}

// ---------------------------------------------------------------------------------------------------------------------
