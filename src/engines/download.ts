//----------------------------------------------------------------------------------------------------------------------
// DownloadEngine
//----------------------------------------------------------------------------------------------------------------------

import { join } from 'node:path';
import tmp from 'temporary';
import logging from '@strata-js/util-logging';

// Interfaces
import { AlbumTags, SetList, SongDetails } from '../interfaces/media';

// Resource Access
import filesRA from '../resource-access/files';
import mediaRA from '../resource-access/media';
import youtubeRA from '../resource-access/youtube';

//----------------------------------------------------------------------------------------------------------------------

const logger = logging.getLogger('download-engine');

//----------------------------------------------------------------------------------------------------------------------

class DownloadEngine
{
    buildBaseDir(albumDetails : AlbumTags) : string
    {
        return join(albumDetails.artist, albumDetails.album);
    }

    buildSongPath(song : SongDetails) : string
    {
        const trackNum = (`${ song.trackNumber }`).padStart(2, '0');

        return join(`${ trackNum } - ${ song.title.replace( /[<>:"\/\\|?*#$%]+/g, '' ) }.mp3`);
    }

    async downloadSetList(setList : SetList, outputDir : string, video ?: string, cache ?: boolean) : Promise<void>
    {
        const baseDir = join(outputDir, this.buildBaseDir(setList.tags));
        const cacheDir = join(outputDir, '_cache');

        // Ensure the basedir exists
        logger.debug(`Ensuring directory: '${ baseDir }'`);
        await filesRA.mkdir(baseDir);

        // Ensure the cacheDir exists
        logger.debug(`Ensuring directory: '${ cacheDir }'`);
        await filesRA.mkdir(cacheDir);

        // TODO: We should default to using the temp directory location as the cache directory, but if there's no
        //  cache, we also use the random tempfile name. Then we default to having a cache, but it's in the temp
        //  directory, which means it'll die eventually, but the user doesn't have to manually clean it.

        // Create a temporary file to use for downloading and converting.
        const tempFile = new tmp.File();

        // This is the path to the cached video file, if it exists
        const cacheFile = join(cacheDir, `${ setList.tags.album.replace( /[<>:"\/\\|?*#$%]+/g, '' ) }.mp4`);

        // If we have a cache, and the file exists, use it.
        if(cache && await filesRA.exists(cacheFile))
        {
            logger.debug('Using cached video file...');
            video = cacheFile;
        }

        // This is the path to the video file, either passed in, or the temp file.
        const videoFile = video ?? cache ? cacheFile : tempFile.path;

        // If we have a video file, use that, otherwise, download the YouTube video.
        if(video)
        {
            logger.info(`Using video file '${ video }'...`);
        }
        else
        {
            logger.info(`Downloading YouTube video '${ setList.video }' to '${ videoFile }'...`);

            // Download the YouTube file to a temporary file.
            await youtubeRA.download(setList.video, cache ? cacheFile : tempFile.path);
        }

        // Loop over each song
        for(const song of setList.songs)
        {
            // We follow a format of `<Artist>/<Album>/<TrackNumber> - <Title>.mp3`
            const outputFile = join(baseDir, this.buildSongPath(song));

            let mp3File : string;
            if(song.alternativeAudio)
            {
                logger.info(`Downloading alternative audio for '${ outputFile }'...`);

                const tempAudioFile = new tmp.File();

                // Download alternative audio
                await mediaRA.downloadMedia(song.alternativeAudio, tempAudioFile.path);

                logger.info(`Building song '${ outputFile }'...`);

                // Convert media to mp3
                mp3File = await mediaRA.convertToMp3(tempAudioFile.path, outputFile);
            }
            else
            {
                logger.info(`Building song '${ outputFile }'...`);

                // Convert section of YouTube video to mp3
                mp3File = await mediaRA.convertToMp3(videoFile, outputFile, song.start, song.end);
            }

            logger.debug('...Writing tags...');

            let artist = song.artists?.join(' & ') ?? setList.tags.artist;

            // Write IDv3 tags
            await mediaRA.writeId3Tags(mp3File, {
                artist,
                album: setList.tags.album,
                image: setList.tags.image,
                originalArtist: song.originalArtist,
                releaseDate: setList.tags.releaseDate,
                title: song.title,
                trackNumber: song.trackNumber,
                video: setList.video,
                year: setList.tags.year
            });

            logger.debug('...song completed.');
        }

        // Remove temp file
        tempFile.unlinkSync();

        // Wait a second to let stdout finish flushing
        await new Promise((resolve) => setTimeout(resolve, 1000));
    }
}

//----------------------------------------------------------------------------------------------------------------------

export default new DownloadEngine();

//----------------------------------------------------------------------------------------------------------------------
