// ---------------------------------------------------------------------------------------------------------------------
// Media Interfaces
// ---------------------------------------------------------------------------------------------------------------------

export interface SongDetails
{
    title : string;
    artists ?: string[];
    originalArtist : string;
    alternativeAudio ?: string;
    start : string;
    end : string;
    trackNumber : number;
}

export interface AlbumTags
{
    artist : string;
    album : string;
    year : number;
    releaseDate : string;
    image ?: {
        mime : string;
        url : string;
    }
}

export interface SetList
{
    video : string;
    tags : AlbumTags;
    songs : SongDetails[];
}

// ---------------------------------------------------------------------------------------------------------------------
