// ---------------------------------------------------------------------------------------------------------------------
// Files Resource Access
// ---------------------------------------------------------------------------------------------------------------------

import { access, constants, readFile, mkdir, writeFile } from 'node:fs/promises';
import { resolve } from 'node:path';

// ---------------------------------------------------------------------------------------------------------------------

class FileResourceAccess
{
    async exists(filePath : string) : Promise<boolean>
    {
        try
        {
            await access(filePath, constants.F_OK);
            return true;
        }
        catch
        {
            return false;
        }
    }

    async mkdir(dirPath : string) : Promise<void>
    {
        await mkdir(dirPath, { recursive: true });
    }

    async readFileAsString(filePath : string) : Promise<string>
    {
        // Resolve to a full path
        filePath = resolve(filePath);

        if(!await this.exists(filePath))
        {
            throw new Error(`File '${ filePath }' does not exist!`);
        }

        return readFile(filePath, { encoding: 'utf8' });
    }

    async writeFile(filePath : string, contents : string | Buffer) : Promise<void>
    {
        // Resolve to a full path
        filePath = resolve(filePath);

        await writeFile(filePath, contents);
    }
}

// ---------------------------------------------------------------------------------------------------------------------

export default new FileResourceAccess();

// ---------------------------------------------------------------------------------------------------------------------
