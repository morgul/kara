//----------------------------------------------------------------------------------------------------------------------
// YouTubeResourceAccess
//----------------------------------------------------------------------------------------------------------------------

import ytdl from 'ytdl-core';
import cliProgress from 'cli-progress';
import { createWriteStream } from 'node:fs';

// Utils
import { formatBytes } from '../utils/format';

//----------------------------------------------------------------------------------------------------------------------

export type YouTubeDownloaderQualityOptions = 'highest' | 'lowest' | 'highestaudio' | 'lowestaudio' | string;

export interface YouTubeDownloadOptions
{
    quality : YouTubeDownloaderQualityOptions
}

const defaultOptions : YouTubeDownloadOptions = {
    quality: 'highestaudio'
};

//----------------------------------------------------------------------------------------------------------------------

class YouTubeResourceAccess
{
    async download(url : string, filePath : string, options : YouTubeDownloadOptions = defaultOptions) : Promise<void>
    {
        let downloadStarted = false;

        options = {
            ...defaultOptions,
            ...options
        };

        // Wait a second to let stdout finish flushing
        await new Promise((resolve) => setTimeout(resolve, 1000));

        // Put some space between us and any log messages
        console.log('');

        // Progress Bar
        const dlProgressBar = new cliProgress.SingleBar(
            {
                clearOnComplete: true,
                format: 'Download Video: [{bar}] {percentage}% | ETA: {eta_formatted} | {dl_formatted} / {total_formatted}'
            },
            cliProgress.Presets.shades_legacy
        );

        // Download the track
        const videoStream = ytdl(url, {
            quality: options.quality
        });

        // Create a promise to know when the stream is complete.
        const writtenPromise = new Promise((done, reject) =>
        {
            videoStream.on('error', reject);

            videoStream.on('progress', (_chunkLength, downloaded, total) =>
            {
                if(!downloadStarted)
                {
                    dlProgressBar.start(total, downloaded, {
                        dl_formatted: formatBytes(downloaded),
                        total_formatted: formatBytes(total)
                    });
                    downloadStarted = true;
                }

                dlProgressBar.update(downloaded, {
                    dl_formatted: formatBytes(downloaded),
                    total_formatted: formatBytes(total)
                });
            });

            videoStream.on('end', () =>
            {
                dlProgressBar.update(100);
                dlProgressBar.stop();
                done(undefined);
            });
        });

        // Pipe to a write stream
        videoStream.pipe(createWriteStream(filePath));

        // Wait for this to be finished.
        await writtenPromise;
    }
}

//----------------------------------------------------------------------------------------------------------------------

export default new YouTubeResourceAccess();

//----------------------------------------------------------------------------------------------------------------------
