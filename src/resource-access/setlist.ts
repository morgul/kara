// ---------------------------------------------------------------------------------------------------------------------
// SetList Resource Access
// ---------------------------------------------------------------------------------------------------------------------

import { parse } from 'yaml';

// Interfaces
import { AlbumTags, SetList, SongDetails } from '../interfaces/media';

// Resource Access
import filesRA from './files';

// ---------------------------------------------------------------------------------------------------------------------

class SetListResourceAccess
{
    async load(filePath : string) : Promise<SetList>
    {
        // Load the set list file.
        const setListStr = await filesRA.readFileAsString(filePath);

        // Parse the object that is, hopefully, a set list.
        const setList = parse(setListStr);

        // -------------------------------------------------------------------------------------------------------------
        // Validate SetList
        // -------------------------------------------------------------------------------------------------------------

        if(!setList.video)
        {
            throw new Error(`SetList file must contain 'video' key.`);
        }

        if(!setList.songs || !Array.isArray(setList.songs))
        {
            throw new Error(`SetList file must contain 'songs' key that is a list.`);
        }

        // Build correct album tags
        const tags : AlbumTags = {
            album: setList.tags.album ?? 'Unknown Album',
            artist: setList.tags.artist ?? 'Unknown Artist',
            releaseDate: setList.tags.releaseDate ?? (new Date()).toLocaleDateString(),
            year: setList.tags?.year ?? (new Date()).getFullYear()
        };

        if(setList.tags.image)
        {
            if(!setList.tags.image.url)
            {
                throw new Error(`SetList file must contain 'url' if it contains an 'image' key!`);
            }

            if(!setList.tags.image.mime)
            {
                throw new Error(`SetList file must contain 'mine' if it contains an 'image' key!`);
            }

            tags.image = {
                url: setList.tags.image.url,
                mime: setList.tags.image.mime
            };
        }

        // Build correct songs list
        const songs : SongDetails[] = (setList.songs ?? []).map((song : Record<string, any>, index : number) =>
        {
            if(!song.start)
            {
                throw new Error(`SetList song #${ index + 1 } has no 'start' key!`);
            }

            if(!song.end)
            {
                throw new Error(`SetList song #${ index + 1 } has no 'end' key!`);
            }

            return {
                title: song.title ?? 'Unknown Title',
                artists: song.artists,
                originalArtist: song.originalArtist ?? 'Unknown Artist',
                alternativeAudio: song.alternativeAudio,
                start: song.start,
                end: song.end,
                trackNumber: song.trackNumber ?? index + 1
            } satisfies SongDetails;
        });

        // Return validated SetList
        return {
            video: setList.video,
            tags,
            songs
        };
    }
}

// ---------------------------------------------------------------------------------------------------------------------

export default new SetListResourceAccess();

// ---------------------------------------------------------------------------------------------------------------------
