//----------------------------------------------------------------------------------------------------------------------
// MediaResourceAccess
//----------------------------------------------------------------------------------------------------------------------

import axios from 'axios';
import { Promise as nodeID3 } from 'node-id3';
import ffmpeg from 'fluent-ffmpeg';
import cliProgress from 'cli-progress';

// Resource Access
import filesRA from './files';

//----------------------------------------------------------------------------------------------------------------------

export interface TagDetails
{
    video : string;
    title : string;
    artist : string;
    album : string;
    year : number;
    originalArtist ?: string;
    releaseDate : string;
    trackNumber : number;
    image ?: {
        mime : string;
        url : string;
    }
}

//----------------------------------------------------------------------------------------------------------------------

class MediaResourceAccess
{
    imageCache : Map<string, Buffer> = new Map();

    async downloadMedia(url : string, outputFile : string) : Promise<void>
    {
        // Progress Bar
        const convProgressBar = new cliProgress.SingleBar(
            {
                clearOnComplete: true,
                format: 'Downloading Media: [{bar}] {percentage}% | ETA: {eta_formatted}'
            },
            cliProgress.Presets.shades_legacy
        );

        // Start the progress bar
        convProgressBar.start(100, 0);

        const { data } = await axios.get(url, {
            responseType: 'arraybuffer',
            onDownloadProgress: (progress) =>
            {
                convProgressBar.update(Math.floor(progress.loaded / (progress.total ?? 0) * 100));
            }
        });

        // Stop the progress bar
        convProgressBar.update(100);
        convProgressBar.stop();

        await filesRA.writeFile(outputFile, Buffer.from(data, 'binary'));
    }

    /**
     * Takes in a path to a file and converts that file to an MP3.
     *
     * @param inputFile - the file to convert to an MP3.
     * @param outputFile - the output file to save to.
     * @param startTime - The timestamp to start the file from.
     * @param endTime - The timestamp to end the file at.
     *
     * @returns Returns a new path to the created MP3.
     */
    async convertToMp3(inputFile : string, outputFile : string, startTime ?: string, endTime ?: string) : Promise<string>
    {
        // Ensure we're ending with .mp3
        if(!outputFile.endsWith('.mp3'))
        {
            outputFile += '.mp3';
        }

        await new Promise<void>((resolve, reject) =>
        {
            ffmpeg.ffprobe(inputFile, (err, metadata) =>
            {
                if(err)
                {
                    return reject(err);
                }

                // Progress Bar
                const convProgressBar = new cliProgress.SingleBar(
                    {
                        clearOnComplete: true,
                        format: 'Building MP3: [{bar}] {percentage}% | ETA: {eta_formatted}'
                    },
                    cliProgress.Presets.shades_legacy
                );

                const audioStream = metadata.streams.find((stream : any) => stream.codec_type === 'audio');
                if(!audioStream)
                {
                    return reject(new Error('Input file does not contain audio stream.'));
                }

                const command = ffmpeg(inputFile)
                    .outputOption('-c:a libmp3lame')
                    .output(outputFile);

                if(startTime)
                {
                    command.seekInput(startTime);
                }

                if(endTime)
                {
                    command.inputOption(`-to ${ endTime }`);
                }

                command.on('start', (_cmdLine) =>
                {
                    convProgressBar.start(100, 0);
                    // Uncomment to do some debugging
                    // console.log('ffmpeg cmd:', _cmdLine);
                });

                command.on('progress', (progress) =>
                {
                    convProgressBar.update(Math.floor(progress.percent));
                });

                command.on('end', () =>
                {
                    convProgressBar.update(100);
                    convProgressBar.stop();
                    resolve();
                });

                command.on('error', (err) =>
                {
                    convProgressBar.stop();
                    reject(err);
                });

                // Give logger time to settle
                setTimeout(() =>
                {
                    command.run();
                }, 1000);
            });
        });

        // Might've changed thanks to setting the `.mp3` extension
        return outputFile;
    }

    /**
     * Writes IDv3 tags to a mp3, based on the TagDetails object provided.
     *
     * @param filePath - The path of the mp3 to update.
     * @param details - The tags to apply to the mp3 file.
     */
    async writeId3Tags(filePath : string, details : TagDetails) : Promise<void>
    {
        const commentText = `Downloaded from YouTube. ${ details.video }`;

        const tags : Record<string, any> = {
            title: details.title,
            artist: details.artist,
            album: details.album,
            performerInfo: details.artist,
            year: `${ details.year }`,
            releaseDate: details.releaseDate,
            trackNumber: `${ details.trackNumber }`
        };

        if(details.originalArtist)
        {
            tags.originalArtist = details.originalArtist;
        }

        if(details.image)
        {
            let imageBuf = this.imageCache.get(details.image.url);
            if(!imageBuf)
            {
                const { data } = await axios.get(details.image.url, { responseType: 'arraybuffer' });
                if(data)
                {
                    imageBuf = Buffer.from(data, 'binary');
                    this.imageCache.set(details.image.url, imageBuf);
                }
            }

            tags.image = {
                mime: details.image.mime,
                type: {
                    id: 3,
                    name: 'front cover'
                },
                description: 'album art',
                imageBuffer: imageBuf
            };
        }

        tags.fileUrl = details.video;
        tags.userDefinedUrl = [
            {
                description: 'original_url',
                url: details.video
            }
        ];

        tags.comment = {
            language: 'eng',
            text: commentText
        };

        await nodeID3.write(tags, filePath);
    }
}

//----------------------------------------------------------------------------------------------------------------------

export default new MediaResourceAccess();

//----------------------------------------------------------------------------------------------------------------------
