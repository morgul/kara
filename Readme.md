# Kara

This is a simple script to take a YouTube video and chop it into multiple mp3s based on a timestamp file. Intended 
to be used for Neuro-sama Karaoke streams, but probably good enough for any other streams.

## Usage

You are required to write out a set list file to tell the script where the songs begin/end as well as how to tag the 
resulting mp3s.

After building one, you can just simply run the script:

```bash
$ node ./dist/kara.js -f ./karaoke-5-7-23.yml -o ~/Music
```

### Set list file

This is just a YAML file in the following format:

```yml
# The video of the archive. A good channel for these is: https://www.youtube.com/@NArchiver
video: https://www.youtube.com/watch?v=DRVGtVHtQN8

# These are the metadata tags
tags:
  artist: 'Neuro-sama'
  album: 'Neuro-sama v3 Karaoke! - 05 July 2023'
  year: 2023
  releaseDate: '7-5-2023'
  image:
    mime: 'image/jpeg'
    url: https://yt3.googleusercontent.com/OiaJrMYn4NeVv_b9QeihVvkgzecWLKE_4Bnpa-t582mTtj9dBjsecF05b2RmLIGKDyajPmv-=s900-c-k-c0x00ffffff-no-rj

# These are the songs from the karaoke stream
songs:
  - title: 'Believer'
    originalArtist: 'Imagine Dragons'
    start: '11:33'
    end: '15:00'
  - title: 'Hero'
    originalArtist: 'YOASOBI'
    start: '17:37'
    end: '20:00'
```

## Development

While contributions are welcome, I doubt anyone but me will really see this. Feel free to drop a question or a bug 
report or a PR if you feel inclined.
